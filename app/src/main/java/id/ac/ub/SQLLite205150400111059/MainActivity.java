package id.ac.ub.SQLLite205150400111059;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    Button bt1;
    Button bt2;
    EditText et1;
    EditText et2;
    TextView tv1;
    private AppDatabase appDb;

    ArrayList<Item> item;
    ItemAdapter adapter;
    RecyclerView rv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        appDb = AppDatabase.getInstance(getApplicationContext());
        bt1 = findViewById(R.id.bt1);
        bt2 = findViewById(R.id.bt2);
        et1 = findViewById(R.id.et1);
        et2 = findViewById(R.id.et2);

        rv1 = findViewById(R.id.rv1);
        rv1.setLayoutManager(new LinearLayoutManager(this));

        bt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Item item = new Item();
                item.setNama(et1.getText().toString());
                item.setNIM(et2.getText().toString());
                AppExecutors.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        appDb.itemDao().insertAll(item);
                    }
                });
            }
        });

        bt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppExecutors.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        AsyncExample task = new AsyncExample(rv1);
                        task.execute();
                    }
                });
            }
        });
    }

    private class AsyncExample extends AsyncTask<Void, Void, List<Item>> {

        RecyclerView rv1;

        public AsyncExample(RecyclerView rv1) {
            this.rv1 = rv1;
        }

        @Override
        protected List<Item> doInBackground(Void... voids) {
            List<Item> list = appDb.itemDao().getAll();
            return list;
        }

        @Override
        protected void onPostExecute(List<Item> items) {
            super.onPostExecute(items);
            adapter = new ItemAdapter(MainActivity.this, items);
            rv1.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }
    }
}
